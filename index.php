    <?php
    require ('animal.php');
    require ('Ape.php');
    require ('Frog.php');
    $sheep = new Animal("shaun");

    echo "Animal : $sheep->name <br>"; // "shaun"
    echo "Legs: $sheep->legs <br>"; // 2
    echo "Cold blooded: $sheep->cold_blooded <br><br>" ;// false
    
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
    // index.php
    $sungokong = new Ape("kera sakti");
    echo "Animal: $sungokong->name <br>";
    echo "Legs: $sungokong->legs <br>"; 
    echo "Sound: ";
    $sungokong-> yell(); // "Auooo"
    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo "Animal: $kodok->name <br>";
    echo "Legs: $kodok->legs <br>"; 
    echo "Jump: ";
    $kodok-> jump(); // "hop hop"
    ?>